#!/bin/bash
#    This file is part of Analytics Environment.
#    Copyright (C) 2020 TUM/MRI
#
#    Analytics Environment is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Analytics Environment is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Analytics Environment.  If not, see <http://www.gnu.org/licenses/>.
#
#    Authors: Helmut Spengler


if [ -z $REL_ID ]; then
  echo "The environment variable \$REL_ID is not set. It needs to be set before being able to run this script!"
  exit 1
else
  echo "\$REL_ID=$REL_ID"
fi

# Basic parameters
I2B2_VER=1.7.09c
TM_VER=16.3
PW_LENGTH=10
DOCKER_REGISTRY_PREFIX=

# Pull gateway, if necessary
if [[ "$(docker images ${DOCKER_REGISTRY_PREFIX}gateway:$REL_ID -q | wc -l)" -lt "1" ]]; then
  echo "Pulling Docker image ${DOCKER_REGISTRY_PREFIX}gateway:$REL_ID"
  docker pull ${DOCKER_REGISTRY_PREFIX}gateway:$REL_ID
else
  echo "Docker image ${DOCKER_REGISTRY_PREFIX}gateway:$REL_ID is already here. Nothing to do"
fi

# Define function for automated password generation
pwgen_f ()  {
    PW_LENGTH=$1
    docker run --rm -t ${DOCKER_REGISTRY_PREFIX}pwgen:${REL_ID} -s $PW_LENGTH 1 | sed -e "s/\r//"
}


if [ "$#" -lt 2 ]; then
	echo "Usage: $0 <Server-CN> <Instance-Number> [--overwrite], e.g. $0 analytics-server.example.org 01"
	exit 0
else
	BASE_HOST_NAME=$1
	INSTANCE_NUM=$2
	if [[ ! -z "$3" ]]; then
		if [[ "$3" == "--overwrite" ]]; then
			OVERWRITE=true
		else
			echo "Third command line parameter (if exists) needs to be '--overwrite', but was $3."
			exit 1
		fi
	fi
fi

BASE_PORT_WEB=80
BASE_PORT_DB=5432
BASE_PORT_AJP=8009
BASE_PORT_REST=8080
BASE_PORT_REST_I2B2=9090
GATEWAY_DIR=./gateway

for i in $INSTANCE_NUM; do

    export HOST_NAME=dwh${i}.${BASE_HOST_NAME}
    
    
    export DB_PORT_I2B2=$(($BASE_PORT_DB + ${i#0}))
    
    if [ -a i2b2-${i}.env ]; then
    	if [ -z "$OVERWRITE" ]; then
    		echo "File i2b2-${i}.env exists. Skipping"
    		SKIP_I2B2=true
    	else
    		echo "Backing up i2b2-${i}.env to i2b2-${i}.env.$$.bak before overwriting."
    		cp i2b2-${i}.env i2b2-${i}.env.$$.bak
    	fi
    fi
    if [ -z "$SKIP_I2B2" ]; then
        echo "Generating env file for i2b2 and generating secure passwords. This can take a minute."
        cat <<- EOF > i2b2-${i}.env
		COMPOSE_PROJECT_NAME=i2b2-$i
		
		IMAGE_APP=${DOCKER_REGISTRY_PREFIX}i2b2-app:${I2B2_VER}-${REL_ID}
		IMAGE_DB=${DOCKER_REGISTRY_PREFIX}i2b2-db:${I2B2_VER}-${REL_ID}
		IMAGE_HTTP=${DOCKER_REGISTRY_PREFIX}i2b2-http:${I2B2_VER}-${REL_ID}
		
		# Do NOT use localhost or 127.0.0.1 here
		DWH_HOST=$HOST_NAME
		
		# Allowed ETL Hosts in CIDR notation, separated by semicolon
		#ALLOWED_ETL_HOSTS=192.168.0.42/32;192.168.178.44/32
		
		# TODO check if PORT_WEBUI, PORT_DB, PORT_REST are needed any longer after dockerization of gateway
		# Port of WebUI
		PORT_WEBUI=$BASE_PORT_WEB
		
		# DB-Port
		PORT_DB=$DB_PORT_I2B2
		
		# REST-Port
		PORT_REST=$BASE_PORT_REST_I2B2
		
		##Database passwords##
		I2B2HIVE_PW=`pwgen_f $PW_LENGTH`
		I2B2DEMODATA_PW=`pwgen_f $PW_LENGTH`
		I2B2IMDATA_PW=`pwgen_f $PW_LENGTH`
		I2B2PM_PW=`pwgen_f $PW_LENGTH`
		I2B2WORKDATA_PW=`pwgen_f $PW_LENGTH`
		I2B2METADATA_PW=`pwgen_f $PW_LENGTH`
		POSTGRES_PW=`pwgen_f $PW_LENGTH`
		
		# Initial passwords for WebUI
		INITIAL_WEBUI_DEMO_PW=`pwgen_f $PW_LENGTH`
		INITIAL_WEBUI_I2B2_PW=`pwgen_f $PW_LENGTH`
		
		# Salt for WebUI Passwords
		HASH_SALT=`pwgen_f $PW_LENGTH`
		HASH_ITERATIONS=1200
	EOF
    fi
    
    # TODO check if AJP_PORT_TRANSMART is needed any longer after dockerization of gateway
    # Generate ENV files for tranSMART
    export AJP_PORT_TRANSMART=$BASE_PORT_AJP
    export DB_PORT_TRANSMART=$(($BASE_PORT_DB + 100 + ${i#0}))
    
    if [ -a transmart-${i}.env ]; then
    	if [ -z "$OVERWRITE" ]; then
    		echo "File transmart-${i}.env exists. Skipping"
    		SKIP_TRAMSMART=true
    	else
    		echo "Backing up transmart-${i}.env to transmart-${i}.env.$$.bak before overwriting."
    		cp transmart-${i}.env transmart-${i}.env.$$.bak
    	fi
    fi
    if [ -z "$SKIP_TRAMSMART" ]; then
        echo "Generating env file for tranSMART and generating secure passwords. This can take a minute."
        	cat <<- EOF > transmart-${i}.env
			COMPOSE_PROJECT_NAME=transmart-$i
			
			IMAGE_TRANSMART=${DOCKER_REGISTRY_PREFIX}transmart:${TM_VER}-${REL_ID}
			
			# Do NOT use localhost or 127.0.0.1 here
			DWH_HOST=$HOST_NAME
			
			# Email address of DWH admin
			EMAIL_ADDR_ADMIN=analytics-support@example.org
			
			# Allowed ETL Hosts in CIDR notation
			#ALLOWED_ETL_HOSTS=192.168.0.42/32
			
			# Port settings
			PORT_DB=$DB_PORT_TRANSMART
			PORT_AJP=$AJP_PORT_TRANSMART
			
			AMAPP_PW=`pwgen_f $PW_LENGTH`
			BIOMART_PW=`pwgen_f $PW_LENGTH`
			BIOMART_STAGE_PW=`pwgen_f $PW_LENGTH`
			BIOMART_USER_PW=`pwgen_f $PW_LENGTH`
			DEAPP_PW=`pwgen_f $PW_LENGTH`
			FMAPP_PW=`pwgen_f $PW_LENGTH`
			GALAXY_PW=`pwgen_f $PW_LENGTH`
			GWAS_PLINK_PW=`pwgen_f $PW_LENGTH`
			I2B2DEMODATA_PW=`pwgen_f $PW_LENGTH`
			I2B2METADATA_PW=`pwgen_f $PW_LENGTH`
			SEARCHAPP_PW=`pwgen_f $PW_LENGTH`
			TM_CZ_PW=`pwgen_f $PW_LENGTH`
			TM_LZ_PW=`pwgen_f $PW_LENGTH`
			TM_WZ_PW=`pwgen_f $PW_LENGTH`
			TS_BATCH_PW=`pwgen_f $PW_LENGTH`
			POSTGRES_PW=`pwgen_f $PW_LENGTH`
			
			# Initial password for user admin for WebUI
			INITIAL_ADMIN_PW=`pwgen_f $PW_LENGTH`
		EOF
    fi
    
    export PORT_REST_I2B2=$((BASE_PORT_REST_I2B2 + ${i#0}))
    # Generate apache config
    if [ -a $GATEWAY_DIR/sites-enabled/dwh-${i}.conf ]; then
    	if [ -z "$OVERWRITE" ]; then
    		echo "File $GATEWAY_DIR/sites-enabled/dwh-${i}.conf exists. Skipping"
    		SKIP_APACHE=true
    	else
    		echo "Backing up $GATEWAY_DIR/sites-enabled/dwh-${i}.conf to $GATEWAY_DIR/sites-enabled/dwh-${i}.conf.$$.bak before overwriting."
    		cp $GATEWAY_DIR/sites-enabled/dwh-${i}.conf $GATEWAY_DIR/sites-enabled/dwh-${i}.conf.$$.bak
    	fi
    fi
    if [ -z "$SKIP_APACHE" ]; then
    cat <<- EOF > $GATEWAY_DIR/sites-enabled/dwh-${i}.conf
	<VirtualHost *:443>
	   ServerName $HOST_NAME
	   
	   # transmart
	   ProxyPass /transmart ajp://transmart-${i}:8009/transmart
	   ProxyPassReverse /transmart ajp://transmart-${i}:8009/transmart
	
	   # i2b2
	   ProxyPass /i2b2/services/ http://i2b2-${i}-app:9090/i2b2/services/
	   ProxyPassReverse /i2b2/services/ http:/i2b2-${i}-app:9090/i2b2/services/
	
	   ProxyPass /i2b2 http://i2b2-${i}-http:80/webclient
	   ProxyPassReverse /i2b2 http://i2b2-${i}-http:80/webclient
	
	   ProxyPass /i2b2-admin http://i2b2-${i}-http:80/admin
	   ProxyPassReverse /i2b2-admin http://i2b2-${i}-http:80/admin
	
	   SSLCertificateFile    /etc/ssl/certs/analytics-server.pem
	   SSLCertificateKeyFile /etc/ssl/private/analytics-server.key
	
	   <Location /transmart>
	     Require all granted
	   </Location>
	
	   <Location /i2b2>
	     Require all granted
	   </Location>
	
	   <Location /i2b2-admin>
	     Require all granted
	   </Location>
	
	   <Proxy "*">
	     Include /etc/apache2/restrict.conf
	   </Proxy>
	   ProxyRequests        Off
	   ProxyPreserveHost    On
	
	   AllowEncodedSlashes NoDecode
	   RequestHeader set X-Forwarded-Proto "https"
	   RequestHeader set X-Forwarded-Port "443"
	</VirtualHost>
	<VirtualHost *:80>
	   RequestHeader set X-Forwarded-Proto "http"
	   RequestHeader set X-Forwarded-Port "80"
	</VirtualHost>
	EOF
    
    fi
    
    # Make sure that only the owner can read the passwords
    if [ -e ".env" ]; then
    	chmod 600 .env *.env
    fi
done

# Make apache read the new configuration
# Pull gateway, if necessary
if [[ "$(docker ps -a -f name=gateway -f status=running -q | wc -l)" -lt "1" ]]; then
	echo "No gateway container started (yet). Nothing to do."
else
  echo "Restarting gateway container."
  docker restart gateway
fi
