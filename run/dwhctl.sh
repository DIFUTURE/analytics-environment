#!/bin/bash
#    This file is part of Analytics Environment.
#    Copyright (C) 2020 TUM/MRI
#
#    Analytics Environment is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Analytics Environment is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Analytics Environment.  If not, see <http://www.gnu.org/licenses/>.
#
#    Authors: Helmut Spengler


function exit_on_wrong_parameters () {
	echo "Usage $0 i2b2|tm config-number up|down|logs"
	exit 1
}


if [[ $1 == 'i2b2' ]]
then
	if [[ $3 == 'up' ]]
	then
		cp i2b2-$2.env  .env && docker-compose -f compose-i2b2.yml up -d
	elif [ $3 == 'down' ]
	then
		if [[ $4 == '--volumes'  || $4 == '-v' ]]
		then
			REMOVE_VOLUMES=$4
		fi
		cp i2b2-$2.env  .env && docker-compose -f compose-i2b2.yml down $REMOVE_VOLUMES
	elif [ $3 == 'logs' ]
	then
		if [[ $4 == '-f' || $4 == '--follow' ]]
                then
                        FOLLOW=-f
                fi
		cp i2b2-$2.env  .env && docker-compose -f compose-i2b2.yml logs $FOLLOW
	else
		exit_on_wrong_parameters
	fi
elif [[ $1 == 'tm' ]]
then	
	if [[ $3 == 'up' ]]
	then
		cp transmart-$2.env  .env && docker-compose -f compose-transmart.yml up -d
	elif [ $3 == 'down' ]
	then
		if [[ $4 == '--volumes'  || $4 == '-v' ]]
		then
			REMOVE_VOLUMES=$4
		fi
		cp transmart-$2.env  .env && docker-compose -f compose-transmart.yml down $REMOVE_VOLUMES
	elif [ $3 == 'logs' ]
	then
		if [[ $4 == '-f' || $4 == '--follow' ]]
                then
                        FOLLOW=-f
                fi
		cp transmart-$2.env  .env && docker-compose -f compose-transmart.yml logs $FOLLOW
	else
		exit_on_wrong_parameters
	fi
else
	exit_on_wrong_parameters
fi	
