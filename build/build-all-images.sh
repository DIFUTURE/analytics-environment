#!/bin/sh
#    This file is part of Analytics Environment.
#    Copyright (C) 2020 TUM/MRI
#
#    Analytics Environment is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Analytics Environment is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Analytics Environment.  If not, see <http://www.gnu.org/licenses/>.
#
#    Authors: Helmut Spengler

if [ -z $REL_ID ]; then
  echo "The environment variable \$REL_ID is not set. It needs to be set before being able to run this script and should correspond to the Git-Tag you have checked out, e.g., 'r5'!"
  exit 1
else
  echo "\$REL_ID=$REL_ID"
fi

# Exit this script on first error
set -e

echo "Building gateway"
docker build --pull ${NO_CACHE} -t gateway:${REL_ID} gateway

echo "Building pwgen"
docker build --pull ${NO_CACHE} -t pwgen:${REL_ID}   pwgen

./build-i2b2.sh 1.7.09c app
./build-i2b2.sh 1.7.09c db
./build-i2b2.sh 1.7.09c http
./build-i2b2.sh 1.7.12a app
./build-i2b2.sh 1.7.12a db
./build-i2b2.sh 1.7.12a http
./build-tm.sh   16.3
