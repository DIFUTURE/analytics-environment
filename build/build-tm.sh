#!/bin/sh
#    This file is part of Analytics Environment.
#    Copyright (C) 2020 TUM/MRI
#
#    Analytics Environment is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    Analytics Environment is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with Analytics Environment.  If not, see <http://www.gnu.org/licenses/>.
#
#    Authors: Helmut Spengler

# Exit this script on first error
set -e

if [ -z $REL_ID ]; then
  echo "The environment variable \$REL_ID is not set. It needs to be set before being able to run this script!"
  exit 1
else
  echo "\$REL_ID=$REL_ID"
fi

export TM_VER=$1

echo "Building transmart:${TM_VER}-${REL_ID}"

docker build --pull ${NO_CACHE} -t transmart:${TM_VER}-${REL_ID}   transmart-${TM_VER}
