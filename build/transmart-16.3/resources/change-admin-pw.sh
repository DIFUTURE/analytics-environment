#!/bin/bash

CURL_LOGIN_RESPONSE_FILE=curl-login-response.txt

echo "Setting initial password for user 'admin'"

printf "Waiting for tranSMART to come up. Using URL  http://${HOSTNAME}:8080/transmart/"
until $(curl --max-time 1 --output /dev/null --silent --head --fail http://${HOSTNAME}:8080/transmart/); do
    printf '.'
    sleep 1
done

# Login
curl -vs \
-H "Host: ${HOSTNAME}:8080" \
-H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8" \
-H "Accept-Language: de,en-US;q=0.7,en;q=0.3" \
-H "Accept-Encoding: gzip, deflate" \
-H "Content-Type: application/x-www-form-urlencoded" \
-H "Connection: keep-alive" \
-H "Referer: http://${HOSTNAME}:8080/transmart/login/forceAuth" \
-H "Upgrade-Insecure-Requests: 1" \
-H "Pragma: no-cache" \
-H "Cache-Control: no-cache" \
-d "j_username=admin" \
-d "j_password=admin" \
"http://${HOSTNAME}:8080/transmart/j_spring_security_check" \
&> ${CURL_LOGIN_RESPONSE_FILE}

# Check if login was successful
LANDING_COUNT=`grep -c 'Location: /transmart/userLanding' ${CURL_LOGIN_RESPONSE_FILE}`

if [ $LANDING_COUNT == 1 ]; then

  # Set default password, if necessary
  EFFECTIVE_PW=${INITIAL_ADMIN_PW:-admin}
  
  # Extract TM_SESSION_ID
  TM_SESSION_ID=`grep JSESSIONID ${CURL_LOGIN_RESPONSE_FILE} | \
  sed -e "s#< Set-Cookie: JSESSIONID=##;s#; Path=/transmart/; HttpOnly##"`
  
  # Change Password
  curl -s -o /dev/null \
  -H "Host: ${HOSTNAME}:8080" \
  -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8" \
  -H "Accept-Language: de,en-US;q=0.7,en;q=0.3" \
  -H "Accept-Encoding: gzip, deflate" \
  -H "Content-Type: application/x-www-form-urlencoded" \
  -H "Origin: http://${HOSTNAME}:8080" \
  -H "Connection: keep-alive" \
  -H "Referer: http://${HOSTNAME}:8080/transmart/changeMyPassword/save" \
  -H "Cookie: JSESSIONID=${TM_SESSION_ID}" \
  -H "Upgrade-Insecure-Requests: 1" \
  -H "Pragma: no-cache" \
  -H "Cache-Control: no-cache" \
  -d "oldPassword=admin" \
  -d "newPassword=${EFFECTIVE_PW}" \
  -d "newPasswordRepeated=${EFFECTIVE_PW}" \
  http://${HOSTNAME}:8080/transmart/changeMyPassword/save
  
  # Close open session
  curl -s -o /dev/null \
  -H "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8" \
  -H "Accept-Language: de,en-US;q=0.7,en;q=0.3" \
  -H "Connection: keep-alive" \
  -H "Cookie: JSESSIONID=${TM_SESSION_ID}" \
  -H "Upgrade-Insecure-Requests: 1" \
  -H "Pragma: no-cache" \
  -H "Cache-Control: no-cache" \
  "http://${HOSTNAME}:8080/transmart/login/forceAuth"
  
  # Remove temporary files
  rm ${CURL_LOGIN_RESPONSE_FILE}
fi