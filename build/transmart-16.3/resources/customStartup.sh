#!/bin/bash

sed -i.bak -e "s/#listen_addresses = 'localhost'/listen_addresses = '*'\t/" /etc/postgresql/9.5/main/postgresql.conf

for CIDR_ENTRY in ${ALLOWED_ETL_HOSTS//;/ }; do
cat << EOF >> /etc/postgresql/9.5/main/pg_hba.conf

host    all             tm_cz     ${CIDR_ENTRY} md5
host    all             postgres  ${CIDR_ENTRY} md5
EOF
done

# Start database
start-and-wait-for-database.sh "echo database is up"

# Adjust email address for admin
sed -i.bak -e "s/transmart-discuss@googlegroups.com/${EMAIL_ADDR_ADMIN}/" /usr/share/tomcat7/.grails/transmartConfig/Config.groovy

# Reset passwords
sudo -E -u postgres /home/transmart/transmart-data/reset-passwords.sh
sudo -E -u tomcat7 /home/transmart/transmart-data/change-ds-settings.sh

# Adjust transmartURL in config
sudo -E -u tomcat7 /home/transmart/transmart-data/change-transmartUrl.sh

echo " * Starting SOLR"
 source /home/transmart/transmart-data/vars && make -C /home/transmart/transmart-data/solr start | tee /var/log/transmart/solr.log 2>&1 &
## make -C solr rwg_full_import sample_full_import
#
echo " * Starting Rserve"
sudo -u tomcat7 bash -c 'source /home/transmart/transmart-data/vars; /home/transmart/transmart-data/R/root/bin/R CMD Rserve --save'
#
echo " * Starting tomcat"
service tomcat7 start

/etc/init.d/change-admin-pw.sh

tail -f /var/log/tomcat7/catalina.out
