#!/bin/bash

DEFAULT_PW=zeil7Yoo

psql -a -d transmart -c "ALTER USER amapp WITH PASSWORD '${AMAPP_PW:-${DEFAULT_PW}}'"
psql -a -d transmart -c "ALTER USER biomart WITH PASSWORD '${BIOMART_PW:-${DEFAULT_PW}}'"
psql -a -d transmart -c "ALTER USER biomart_stage WITH PASSWORD '${BIOMART_STAGE_PW:-${DEFAULT_PW}}'"
psql -a -d transmart -c "ALTER USER biomart_user WITH PASSWORD '${BIOMART_USER_PW:-${DEFAULT_PW}}'"
psql -a -d transmart -c "ALTER USER deapp WITH PASSWORD '${DEAPP_PW:-${DEFAULT_PW}}'"
psql -a -d transmart -c "ALTER USER fmapp WITH PASSWORD '${FMAPP_PW:-${DEFAULT_PW}}'"
psql -a -d transmart -c "ALTER USER galaxy WITH PASSWORD '${GALAXY_PW:-${DEFAULT_PW}}'"
psql -a -d transmart -c "ALTER USER gwas_plink WITH PASSWORD '${GWAS_PLINK_PW:-${DEFAULT_PW}}'"
psql -a -d transmart -c "ALTER USER i2b2demodata WITH PASSWORD '${I2B2DEMODATA_PW:-${DEFAULT_PW}}'"
psql -a -d transmart -c "ALTER USER i2b2metadata WITH PASSWORD '${I2B2METADATA_PW:-${DEFAULT_PW}}'"
psql -a -d transmart -c "ALTER USER searchapp WITH PASSWORD '${SEARCHAPP_PW:-${DEFAULT_PW}}'"
psql -a -d transmart -c "ALTER USER tm_cz WITH PASSWORD '${TM_CZ_PW:-${DEFAULT_PW}}'"
psql -a -d transmart -c "ALTER USER tm_lz WITH PASSWORD '${TM_LZ_PW:-${DEFAULT_PW}}'"
psql -a -d transmart -c "ALTER USER tm_wz WITH PASSWORD '${TM_WZ_PW:-${DEFAULT_PW}}'"
psql -a -d transmart -c "ALTER USER ts_batch WITH PASSWORD '${TS_BATCH_PW:-${DEFAULT_PW}}'"
psql -a -d transmart -c "ALTER USER postgres WITH PASSWORD '${POSTGRES_PW:-${DEFAULT_PW}}'"
