#!/bin/bash
# make-postgres.sh

set -e

/etc/init.d/postgresql start

until psql -c '\q' ; do  >&2 echo "DB is unavailable - waiting one more second";  sleep 1; done

>&2 echo "DB is up"
source $TRANSMART_HOME/transmart-data/vars && make -j4 postgres

/etc/init.d/postgresql stop