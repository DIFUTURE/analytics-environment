#!/bin/bash
# wait-for-database.sh

set -e

cmd="$@"

/etc/init.d/postgresql start

printf "Waiting for PostgreSQL to come up "
until su - postgres -c "psql -c '\q'" 2>&1 /dev/null; do  >&2 printf ".";  sleep 1; done

>&2 echo "DB is up - executing command '$cmd'"
$cmd
