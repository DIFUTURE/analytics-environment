/*
 * PWHash
 * Copyright (C) 2020 TUM/MRI
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Base64;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * 
 * @author Florian Kohlmayer (kohlmayer[at]bitcare.de)
 * @author Helmut Spengler (helmut.spengler[at]tum.de)
 *
 */
public class PWHash {

    protected final Log         log             = LogFactory.getLog(getClass());

    public static void main(String[] args) {

        PWHash hash = new PWHash();
        String hashedPW = hash.getHashedPassword(args[0]);

        System.out.println(hashedPW);

    }

    public String getHashedPassword(String pass) {
        try {

            byte[] salt = null;
            String saltString = System.getenv("SALT_ENV");
            // set default salt, if empty
            if (saltString == null || saltString.isEmpty()) {
                saltString = "i2b2Salt";
            }
            salt = saltString.getBytes();

            Integer iterations = null;
            String iterationsString = System.getenv("ITERATIONS_ENV");
            // try to parse
            try {
                iterations = Integer.parseInt(iterationsString);
            } catch (Exception e) {
                // silenty ignore
            }
            // set default iterations, if empty
            if (iterations == null) {
                iterations = 1000;
            }

            char[] passChars = pass.toCharArray();
            PBEKeySpec spec = new PBEKeySpec(passChars, salt, iterations, 64 * 8);
            SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] hash = skf.generateSecret(spec).getEncoded();

            return toBase64(hash);
        } catch (NoSuchAlgorithmException e) {
            log.error("NoSuchAlgorithm PBKDF2WithHmacSHA1!", e);
        } catch (InvalidKeySpecException e) {
            log.error("Invalid key spec!", e);
        }
        return null;
    }

    public static String toBase64(byte[] digest) {
        return Base64.getEncoder().withoutPadding().encodeToString(digest);
    }
}
