CREATE SEQUENCE i2b2demodata.seq_patient_num
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 10
  CACHE 1;
ALTER TABLE i2b2demodata.seq_patient_num
  OWNER TO postgres;


  CREATE SEQUENCE i2b2demodata.seq_encounter_num
  INCREMENT 1
  MINVALUE 1
  MAXVALUE 9223372036854775807
  START 1
  CACHE 1;
ALTER TABLE i2b2demodata.seq_encounter_num
  OWNER TO postgres;
