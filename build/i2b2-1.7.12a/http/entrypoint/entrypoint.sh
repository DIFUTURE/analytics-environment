#!/bin/bash

sed -i "s/\${DWH_HOST}/${DWH_HOST}/g" /var/www/html/webclient/index.php
sed -i "s/\${DWH_HOST}/${DWH_HOST}/g" /var/www/html/admin/index.php
sed -i "s/\${PORT_REST}/${PORT_REST}/g" /var/www/html/webclient/index.php
sed -i "s/\${PORT_REST}/${PORT_REST}/g" /var/www/html/admin/index.php
sed -i "s/i2b2-app/$DWH_HOST/" /var/www/html/admin/i2b2_config_data.js
sed -i 's/loginDefaultPassword : "demouser"/loginDefaultPassword : ""/' /var/www/html/admin/js-i2b2/i2b2_ui_config.js
sed -i "s/i2b2-app/$DWH_HOST/" /var/www/html/webclient/i2b2_config_data.js

sed -i.bak -e 's#/webclient/help/#/i2b2/help/#' /var/www/html/webclient/help/default.htm
sed -i.bak -e 's#/admin/help/#/i2b2-admin/help/#' /var/www/html/admin/help/default.htm

/usr/sbin/apache2ctl -D FOREGROUND
