{
	urlProxy: "index.php",
	urlFramework: "js-i2b2/",
	startZoomed: true,
	//-------------------------------------------------------------------------------------------
	// THESE ARE ALL THE DOMAINS A USER CAN LOGIN TO
	lstDomains: [
		{ domain: "i2b2demo",
		  name: "demo",
		  urlCellPM: "https://i2b2-app/i2b2/services/PMService/",
		  allowAnalysis: true,
          adminOnly: true,
		  debug: false
		}
	]
	//-------------------------------------------------------------------------------------------
}
