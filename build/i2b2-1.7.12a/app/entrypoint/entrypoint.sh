#!/bin/bash

DEFAULT_PW=Phoo4eih

I2B2_DIR=/opt/i2b2-core-server-1.7.12a.0002
sed -i "s/\${I2B2HIVE_PW}/${I2B2HIVE_PW:-${DEFAULT_PW}}/g" ${I2B2_DIR}/edu.harvard.i2b2.crc/etc/jboss/crc-ds.xml
sed -i "s/\${I2B2DEMODATA_PW}/${I2B2DEMODATA_PW:-${DEFAULT_PW}}/g" ${I2B2_DIR}/edu.harvard.i2b2.crc/etc/jboss/crc-ds.xml
sed -i "s/\${I2B2METADATA_PW}/${I2B2METADATA_PW:-${DEFAULT_PW}}/g" ${I2B2_DIR}/edu.harvard.i2b2.ontology/etc/spring/OntologyApplicationContext.xml
sed -i "s/\${I2B2HIVE_PW}/${I2B2HIVE_PW:-${DEFAULT_PW}}/g" ${I2B2_DIR}/edu.harvard.i2b2.crc/etc/spring/CRCLoaderApplicationContext.xml
sed -i "s/\${I2B2HIVE_PW}/${I2B2HIVE_PW:-${DEFAULT_PW}}/g" ${I2B2_DIR}/edu.harvard.i2b2.im/etc/jboss/im-ds.xml
sed -i "s/\${I2B2METADATA_PW}/${I2B2METADATA_PW:-${DEFAULT_PW}}/g" ${I2B2_DIR}/edu.harvard.i2b2.im/etc/jboss/im-ds.xml
sed -i "s/\${I2B2HIVE_PW}/${I2B2HIVE_PW:-${DEFAULT_PW}}/g" ${I2B2_DIR}/edu.harvard.i2b2.ontology/etc/jboss/ont-ds.xml
sed -i "s/\${I2B2METADATA_PW}/${I2B2METADATA_PW:-${DEFAULT_PW}}/g" ${I2B2_DIR}/edu.harvard.i2b2.ontology/etc/jboss/ont-ds.xml
sed -i "s/\${I2B2HIVE_PW}/${I2B2HIVE_PW:-${DEFAULT_PW}}/g" ${I2B2_DIR}/edu.harvard.i2b2.workplace/etc/jboss/work-ds.xml
sed -i "s/\${I2B2WORKDATA_PW}/${I2B2WORKDATA_PW:-${DEFAULT_PW}}/g" ${I2B2_DIR}/edu.harvard.i2b2.workplace/etc/jboss/work-ds.xml
sed -i "s/\${I2B2PM_PW}/${I2B2PM_PW:-${DEFAULT_PW}}/g" ${I2B2_DIR}/edu.harvard.i2b2.pm/etc/jboss/pm-ds.xml
sed -i "s/\${I2B2IMDATA_PW}/${I2B2IMDATA_PW:-${DEFAULT_PW}}/g" ${I2B2_DIR}/edu.harvard.i2b2.im/etc/jboss/im-ds.xml


sed -i "s/\${PORT_REST}/${PORT_REST}/g" ${I2B2_DIR}/edu.harvard.i2b2.workplace/etc/spring/workplace.properties
sed -i "s/\${PORT_REST}/${PORT_REST}/g" ${I2B2_DIR}/edu.harvard.i2b2.im/etc/spring/im.properties
sed -i "s/\${PORT_REST}/${PORT_REST}/g" ${I2B2_DIR}/edu.harvard.i2b2.ontology/etc/spring/ontology.properties
sed -i "s/\${PORT_REST}/${PORT_REST}/g" ${I2B2_DIR}/edu.harvard.i2b2.crc/etc/spring/crc.properties
sed -i "s/\${PORT_REST}/${PORT_REST}/g" ${I2B2_DIR}/edu.harvard.i2b2.crc/etc/spring/edu.harvard.i2b2.crc.loader.properties
sed -i "s/\${PORT_REST}/${PORT_REST}/g" ${I2B2_DIR}/edu.harvard.i2b2.fr/etc/spring/edu.harvard.i2b2.fr.properties
sed -i "s/\${PORT_REST}/${PORT_REST}/g" ${I2B2_DIR}/admin/index.php

sed -i "s/\${INITIAL_AGG_SERVICE_ACCOUNT_PW}/${INITIAL_AGG_SERVICE_ACCOUNT_PW:-demouser}/g" ${I2B2_DIR}/edu.harvard.i2b2.ontology/etc/spring/ontology.properties
sed -i "s/\${INITIAL_AGG_SERVICE_ACCOUNT_PW}/${INITIAL_AGG_SERVICE_ACCOUNT_PW:-demouser}/g" ${I2B2_DIR}/edu.harvard.i2b2.crc/etc/spring/crc.properties

cd ${I2B2_DIR}/edu.harvard.i2b2.ontology
ant -f master_build.xml clean build-all deploy

cd ${I2B2_DIR}/edu.harvard.i2b2.crc
ant -f master_build.xml clean build-all deploy

cd ${I2B2_DIR}/edu.harvard.i2b2.im
ant -f master_build.xml clean build-all deploy

cd ${I2B2_DIR}/edu.harvard.i2b2.pm
ant -f master_build.xml clean build-all deploy

cd ${I2B2_DIR}/edu.harvard.i2b2.ontology
ant -f master_build.xml clean build-all deploy

cd ${I2B2_DIR}/edu.harvard.i2b2.workplace
ant -f master_build.xml clean build-all deploy

cd ${I2B2_DIR}/edu.harvard.i2b2.fr
ant -f master_build.xml clean build-all deploy

cd ${I2B2_DIR}/admin
ant -f master_build.xml clean build-all deploy

/opt/wildfly-17.0.1.Final/bin/standalone.sh -Djboss.http.port=9090 -Djboss.server.log.dir=/var/log/jboss-as -b=0.0.0.0 -bmanagement=0.0.0.0
