# Introduction

This projects contains scripts and configuration files for building and deploying Docker containers which encapsulate instances of the data analytics platforms **i2b2 1.7.09c** and **tranSMART 16.3**. In addition to the agile deployment of these analytics platforms, it covers aspects which important in production environments, but are left open in the standard installations, such as
* transport encryption
* password management
* (non-)existence of demonstration data

## Structure of this project
This project consists of two major parts: one for **building** and one for **running** the respective Docker containers. This structure is reflected by the top-level directory structure and its directories `build` and `run`, respectively.

If you merely want to checkout the functionality of the images or want to use them in your own runtime environment or deployment system, everything you need, including a basic `docker-compose.yml` can be found in the directory `build`. Note that with this very basic setup, transport encryption and multi-instance support needs to be manually configured.

If you want off-the-shelf transport encryption and multi-instance support you should consider using the tools provided in the `run` directory.

## Versioning scheme
The versioning scheme used in this project addresses the fact that each image is related not only to a specific version of analytics platform (e.g., i2b2, Version 1.7.12a), but also reflects a certain maturity level in the development of the provisioning infrastructure itself, including the related Docker containers. Therefore, we append an additional, independent, **release-number** to the version number of the analytics platform, which is incremented by 1, each time a significant set of enhancements of the provisioning infrastructure is released. The tags in the Git repository (`r1`, `r2`, etc.) represent these releases. The `master` of the repository should reflect the code of the latest release (except for merge-commits and hot-fixes).

For optimal use of the provisioning infrastructure, we recommend to set the environment variable `$REL_ID` the according release-number. The versioning scheme is mainly used for tagging the Docker images, e.g., `i2b2-app:1.7.12a-r6`.

# System requirements

### General system requirements

Our infrastructure is designed to run a a single host with a standard Linux operating system. This host can either be a bare-metal or a virtual machine. The DIC of the University Hospital rechts der Isar currently uses Ubuntu 18.04 LTS on top of a QEMU 2.5.0 KVM.

#### Minimal hardware requirements
* 16 GB RAM
* 100 GB HD
* CPU @ 3 GHz

#### Operating system
Recommended OS for production environments is Ubuntu Linux 18.04 LTS, however, the usual (Non-Alpine) Linux  distributions (e.g. Debian, Fedora) should be fine.

#### Software dependencies
* **Docker and Docker Compose:** The basic prerequisite for running the Docker images is Docker CE 18 or higher *and* Docker Compose 18.06 or higher. We strongly recommend following the official Docker installation guide for your OS, which is available here:
  * Docker: https://docs.docker.com/engine/install/
  * Docker Compose: https://docs.docker.com/compose/install/

#### Operating system
For evaluation and development purposes, in addition to the supported Linux distributions for production environments, modern Windows and MacOS versions can be used, however use of Safari as a web browser is discouraged.

### System requirements for production environments
* **DNS entries and TLS certificates:** For host addressing, authentication and transport encryption, valid TLS certificates are needed. For details, we refer to Section [DNS configuration and certificates](#dns-configuration-and-certificates).

#### Users and permissions
After the installation of Docker, a OS user named `docker` needs to be created and subsequently assigned to the (at this point) already existing groups `docker` and `su`. To this end you can use commands similar to `sudo adduser --shell /bin/bash --ingroup docker --ingroup sudo docker && sudo usermod -a -G docker docker`

## System design

### Docker images

The i2b2 service stack comprises three containers:
- **i2b2-db** Database server for i2b2 - based on PostgreSQL 9.5
- **i2b2-app** Application server for i2b2 - based on WildFly 10
- **i2b2-http** Web server for i2b2 - based on Apache HTTP Server

The tranSMART service stack comprises one container:
- **transmart** TranSMART server stack, including Apache Tomcat, Apache Solr, RServ, and PostgreSQL 9.5

### Component interaction

Figure 1 illustrates the the components used by our environment and their interaction. The actual instances of i2b2 and tranSMART are implemented as (stacks of) Docker containers (black boxes). Access to to these containers is relayed by a web server, which acts as a gateway and reverse proxy. Each warehouse instance is represented by an apache virtual. The Docker networks configured in such a way that only the reverse proxy has access to the network interfaces exposed by the containers.

![Schematic overview of different DWH instances](img/DataMartsDockerizedApache.png)

**Figure 1.** Schematic overview of the components responsible for the provisioning of multiple warehouse instances and their interaction.

### Limitations
The infrastructure supports instantiation and maintenance of a maximum of 99 i2b2 and 99 tranSMART instances.

## Building the containers

### Prerequisites / Preparation

As in production environments it is advisable to have the containers running on a different host than the one, on which the images are built, we recommend using a Docker registry server to which the images can be transferred for further dissemination. In cases where the build of the images and their execution is performed on the same host (e.g. for evaluation or development purposes), no local Docker registry is needed. The image placeholders (`IMAGE_DB`, `IMAGE_APP`, etc.)  can be replaced by image names which can either refer to local repositories or to a registry server. In the latter case, make sure to push the built images to the registry server using the `docker push` command.

### Building the images

Export the environment variable $REL_ID to the release you wish to build. This should correspond to the Git-Tag you have checked out, e.g., "r5".

For building the images, `cd` to the directory `build` an execute the following command: 

```
./build-all-images.sh
```

## Operating, and using the infrastructure

While the infrastructure is aimed at being used in production environments, the containers that have been built in the previous step can be used in a simplified setup without the need to provide DNS entries and valid certificates. In fact, we recommend users of this infrastructure to first setup, run and test (incl. ETL) the containers in this simplified setup, described in the next section, before starting to setup the production environment described in the subsequent section.

### Evaluation and development

Before deploying a production setup, we recommend to first set up the simplified evaluation and development environment. Everything needed for this is contained in the directory `build/` (including docker-compose files), so everything contained in the directory `run/` can be ignored for setting up and running a development/evaluation setup. However this setup has following limitations:
* *no* multi-instance support
* *no* transport-encryption (CAVE: adding transport encryption to this setup requires in-depth knowledge of the i2b2 system architecture and can consume a considerable amount of time. If you need transport encryption, we *strongly* recommend using the production setup, as these time-consuming tasks have been automated for this setup)

To set up the development/evaluation environment:
* make sure that ports 80 and 8080 of your host system are available
* `cd` to (`build/i2b2-1.7.9/`) or (`build/transmart-16.3/`)
* Edit the file `.env` and set following parameters
  * `IMAGE*` must correspond to the names of the images that have been built in the previous step.
  * `DWH_HOST` must correspond to name or IP address of the network interface which is used to access the Apache reverse proxy from the end users' web browsers. Do **not** us `localhost` or `127.0.0.1` as value for this variable, as this will lead to ambiguous addressing of the infrastructure compoments and thus to malfunctions. 
  * `ALLOWED_ETL_HOSTS` denotes the address range of hosts that are allowed direct access to the database, typically for performing ETL processes.
* Instantiate and start the service stack with the command `docker-compose up -d`
* Monitor the startup process with the command `docker-compose logs -f`
* Wait until the services are ready for operation. This is typically indicated by log messages containing following data:
  * **i2b2:** `WildFly Full 10.0.0.Final (WildFly Core 2.0.10.Final) started`
  * **tranSMART:** `INFO: Server startup in`
* Login to the warehouses with your web browser using following URLs
  * **i2b2:** `http://${DWH_HOST}/webclient/` (Username: `demo` / password: `${INITIAL_WEBUI_DEMO_PW}`)
  * **tranSMART:**  `http://${DWH_HOST}:8080/transmart/` (Username: `admin` / password: `${INITIAL_ADMIN_PW}`)

### Production

This section describes all necessary steps involved in setting up and operating our analytics environment.

#### Setting up the gateway

```
docker network create dzone
cd run
docker-compose -f compose-gateway.yml up -d
```

#### Setting up warehouse instances

After having acquired and installed the TLS certificates (see [DNS configuration and certificates](#dns-configuration-and-certificates)), `cd` to the directory `run` and execute the following command:

```
./setup-instance.sh <Hostname> <Instance-Name> [--overwrite]
```

**Listing 1.** Setting up a new warehouse instance.

The mandatory parameter `<Hostname>` denotes the host name of the server as described in Section [Accessing warehouse instances](#accessing-warehouse-instances). It will be used as the base name for the virtual host names of the warehouse instances.The mandatory parameter `<Instance-Name>` denotes the identifier of the warehouse instance to be created.  Note that `1` and `01` denote two different instance identifiers. If enclosing this parameter in quotes, it is possible to create multiple warehouse instances in one command (for an example, see Listing 2).

If the configurations for a certain warehouse instance has already been performed and the script is repeatedly executed for the same instance, its default behavior is to leave the existing configuration untouched. This behavior (as well as the existing configuration files) can be overwritten by using the optional switch `--overwrite`. As an example, the following command will create all necessary configurations for warehouse instances `dwh01.example.org` through `dwh03.example.org`, overwriting all potentially existing configurations:


```
./setup-instance.sh example.org '01 02 03' --overwrite
```

**Listing 2.** Setting up multiple warehouse instances at once.

This script essentially creates the virtual host configurations for the reverse proxy and the environment files for the docker-compose scripts. The names of the environment files follow the scheme `i2b2-<Instance-Name>.env` and `transmart-<Instance-Name>.env` for i2b2 and tranSMART, respectively. These files should be considered as starting points for new instances and are intended to be further edited by the system operator. For the configuration options of the reverse proxy, we refer to the official documentaion of the Apache HTTP Server. The environment variables for the docker-compose files constitute a facade for the runtime configuration of the warehouse instances. They are described in detail in the following two sections.

#### Environment variables

<table>
<caption><strong>Table 1. </strong>Environment variables used for both warehouse platforms</caption>
<thead>
<tr class="header">
   <th style="text-align: left;"><strong>Variable</strong></th>
   <th style="text-align: left;"><strong>Description</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
    <td style="text-align: left;"><code>COMPOSE_PROJECT_NAME</code></td>
    <td style="text-align: left;">Used for assembling container names which are used for inter-container communication.</td>
</tr>
<tr class="odd">
    <td style="text-align: left;"><code>DWH_HOST</code></td>
    <td style="text-align: left;">The fully qualified domain name (FQDN) of the application instance.</td>
</tr>
</table>

<table>
<caption><strong>Table 2. </strong>Environment variables for i2b2</caption>
<thead>
<tr class="header">
    <th style="text-align: left;"><strong>Variable</strong></th>
   <th style="text-align: left;"><strong>Description</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
    <td style="text-align: left;"><code>IMAGE_DB<br>IMAGE_APP<br>IMAGE_HTTP</code><br></td>
    <td style="text-align: left;">identifiers for the the various images. These can refer to remote or local re-<br>positories, e.g. <code>registry.example.org/i2b2-1.7.09c-db:release</code> or <code>i2b2-1.7.09c-db:latest</code></td>
  </tr>
  <tr>
<tbody>
<tr class="odd">
    <td style="text-align: left;"><code>ALLOWED_ETL_HOSTS</code></td>
    <td style="text-align: left;">Netmask for list of host that are allowed to establish a direct database connection to the warehouse in CIDR notation.</td>
  </tr>
<tr class="odd">
    <td style="text-align: left;"><code>INITIAL_WEBUI_DEMO_PW<br>INITIAL_WEBUI_I2B2_PW</code><br></td>
    <td style="text-align: left;">Initial application passwords (e. g. for the Web-UIs). Once logged in, users are able to change their password. As soon as this has happened, the passwords specified in the <code>.env</code> file are void.</code></td>
  </tr>
<tr class="odd">
    <td style="text-align: left;"><code>HASH_SALT<br>HASH_ITERATIONS</code><br></td>
    <td style="text-align: left;">Passwords are hashed using the <code>PBKDF2WithHmacSHA1</code> algorithm. These variables allow for defining the salt used for hashing as well as the number of iterations.</code></td>
  </tr>
  <tr>
    <td style="text-align: left;"><code>I2B2HIVE_PW<br>I2B2DEMODATA_PW<br>I2B2IMDATA_PW<br>I2B2PM_PW<br>I2B2WORKDATA_PW<br>I2B2METADATA_PW</code></td>
    <td style="text-align: left;">Database passwords<br></td>
  </tr>
</table>

<table>
<thead>
<tr class="header">
   <th style="text-align: left;"><strong>Variable</strong></th>
   <th style="text-align: left;"><strong>Description</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
    <td style="text-align: left;"><code>IMAGE</code></td>
    <td style="text-align: left;">Identifiers for the the various images. These can refer to remote or local repositories, e.g. <code>registry.example.org/transmart-16.3:release</code> or <code>transmart-16.3:latest</code></td>
</tr>
<tr class="odd">
    <td style="text-align: left;"><code>EMAIL_ADDR_ADMIN</code></td>
    <td style="text-align: left;">Email contact for system administrator. Used on landing page.                                                                                                                                                        </td>
</tr>
<tr class="odd">
    <td style="text-align: left;"><code>INITIAL_ADMIN_PW</code></td>
    <td style="text-align: left;">Initial password for user <code>admin</code> in Web-UI. Once logged in, users are able to change their password. As soon as this has happened, the password specified in the <code>.env</code> file are void.                                                                                                                                                        </td>
</tr>
<tr>
    <td style="text-align: left;"><code>AMAPP_PW<br>BIOMART_PW<br>BIOMART_STAGE_PW<br>BIOMART_USER_PW<br>DEAPP_PW<br>FMAPP_PW<br>GALAXY_PW<br>GWAS_PLINK_PW<br>I2B2DEMODATA_PW<br>I2B2METADATA_PW<br>SEARCHAPP_PW<br>TM_CZ_PW<br>TM_LZ_PW<br>TM_WZ_PW<br>TS_BATCH_PW<br>POSTGRES_PW</code></td>
    <td style="text-align: left;">Database passwords<br></td>
</tr>
</table>


#### Starting and stopping warehouse instances

For starting and stopping application instances, we provided the script `dwhctl.sh`.

```
./dwhctl.sh i2b2|tm <Instance-Name> <up|down|logs>  [--volumes] [--follow]
```

This script essentially is a wrapper for the `docker-compose` command. It copies the set of environment variables that are contained in the according environment files (`i2b2-<Instance-Name>.env`, or `transmart-<Instance-Name>.env`) to the `.env` file and then uses the associated `i2b2.yml` and `transmart.yml` for execution of `docker-compose`. The scripts accepts the following parameters:
* `i2b2|tm` (mandatory): Specifies what type of warehouse to be managed (i2b2, or tranSMART, recpectively)
* `Instance-Name` (mandatory): Specifies the Id of the instance to be managed. It is recommended to use consecutive numbers with leading zeroes (e. g., `05`). The Ids have to match those used in the DNS entries and in the certificates.
* `up|down|logs` (mandatory): Specifies, the activity to perform, i.e.,  whether to start or stop an instance, or to display logging information. The latter two optiona accept additional, optional, parameters.
* `--volumes` (optional): only useful in conjunction with the `down` activity. Automatically removes associated volumes.
* `--follow`: (optional): only useful in conjunction with the `logs` activity. Follow the log output.
 for which the values `up` or `down` are accepted and which has the same semantics as in the `docker-compose` command: if using `up`, the scripts creates and starts the containers that are defined as services in the according Docker-compose files (`i2b2.yml` or `transmart.yml`, respectively). If using `down`, the containers are stopped and deleted; in this case, the optional parameter `--volumes` causes the deletion of all related managed volumes.

#### Accessing warehouse instances

These users can access the instances using the following URLs:

<table>
<caption><strong>Table 4. </strong>URLs for the Web-UIs of the warehouses.</caption>
<thead>
<tr class="header">
   <th style="text-align: left;"><strong>Application</strong></th>
   <th style="text-align: left;"><strong>URL</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
    <td style="text-align: left;">i2b2</td>
    <td style="text-align: left;"><code>https://dwh${Instance-Name}.${Hostname}/i2b2/</code></td>
<tr class="odd">
    <td style="text-align: left;">i2b2 administration</td>
    <td style="text-align: left;"><code>https://dwh${Instance-Name}.${Hostname}/i2b2-admin/</code></td>
</tr>
</tr>
<tr class="odd">
    <td style="text-align: left;">tranSMART
    <td style="text-align: left;"><code>https://dwh${Instance-Name}.${DWH_HOST}/transmart/</code></td>
</tr>
</table>

As an example, the URL `https://dwh02.example.org/i2b2/` denotes the Web-UI of i2b2 instance `02` on the host system `example.org`.

#### Deleting warehouse instances

When removing Docker containers (using the commands `dwhctl.sh down` or `docker-compose down`, the default behavior is that all payload and configuration data (i.e., data contained in attached docker volumes, the according reverse proxy configuration files as well as the environment files) is retained in order to avoid accidental information loss. 

In order to delete data volumes that are no longer needed, the aforementioned commands accept an optional parameter `--volumes`, which forces the removal of all managed volumes for a particular application instance. Alternatively, the command `docker volume rm` can be used. An example can be found in the following listing, which deletes all managed Docker volumes attached to the first i2b2 instance:

```
docker volume rm $(docker volume ls -f "name=i2b2-01" -q)
```

For removing the runtime configuration for the containers, simply remove the environment files as illustrated in the following listing:

```
rm i2b2-<Instance-Name>.env transmart-<Instance-Name>.env
rm gateway/sites-enabled/dwh-<Instance-Name>.conf
```

## Troubleshooting

### When trying to login to the i2b2 webapp, the message *The PM Cell is down or the address in the properties file is incorrect* appears
#### Possible reason 1
The application server might still be starting up.
#### Solution
Check docker logs:
* Development environment: in directory `build/i2b2-1.7.09c/` or `build-transmart-16.3/`, execute the command `docker-compose logs -f`
* Production environment: in directory `run/`, execute the command
  * i2b2: `docker-compose -f i2b2.yml; logs -f`
  * tranSMART: `docker-compose -f transmart.yml logs -f`
  
Wait until the log output a message containing following data:
* i2b2:** `WildFly Full 10.0.0.Final (WildFly Core 2.0.10.Final) started`
* tranSMART:** `INFO: Server startup in`

#### Possible reason 2
Use of `localhost` or `127.0.0.1` (or another system coordinate that is not accessible from the end users web browser) for environment variable `DWH_HOST`.
#### Solution
Replace variable content by name or address that can be accessed from the end users the web browser, e.g. the IP address or DNS name of the network card of the host system.

### When trying to login to the i2b2 webapp, the message *ERROR: Database error in getting environment data* appears
#### Possible reason
The database system failed to start. Check with the command `docker exec i2b2-db service postgresql status`.
#### Solution
Start the database using the command `docker exec i2b2-db service postgresql start`.

### The warehouse instances cannot be reached due to IP routing problems
#### Possible reason
The IP address ranges of your local network (or VPN) clash with the default address ranges assigned by the Docker engine to your containers.
#### Solution (kudos to Marco Johns and rNix)
Configure Docker engine to use a non clashing address range, e.g., by editing `/etc/docker.daemon.json` to contain following configuration directive


```
{
  "bip": "192.168.1.1/24",
  "default-address-pools":[
    {"base":"192.168.2.0/16","size":24}
  ]
}
```
For further details, see https://serverfault.com/questions/916941/configuring-docker-to-not-use-the-172-17-0-0-range

## Implementation details

### DNS configuration and certificates

The setup of the reverse proxy relies on the different data warehouse instances sharing one IP address. For clients to find this address, your DNS server has to be configured to resolve the DNS names of the warehouse instances (`dwh01.example.org`, `dwh02.example.org`, etc.) accordingly.

In order to simplify the TLS configuration, we make use of the *Subject Alternative Name* (SAN) extension to the X.509 server certificates , which we use for authenticating the data warehouses and for transport encryption. Using this extension allows for sharing one server certificate among the different data warehouse instances on one server. Listing 3 contains an example configuration for creating a *Certificate Signing Request* (CSR) for obtaining such a certificate. Lines 18 through 22 constitute the SAN configuration of this CSR.

``` numberLines
prompt = no
distinguished_name = req_distinguished_name

[req]
req_extensions = v3_req

[ req_distinguished_name ]
organizationalUnitName = Example Unit
organizationName = Example Organization
localityName = Example City
stateOrProvinceName = Example State
countryName = DE
emailAddress = certadmin@example.org

commonName = dwh.example.org

[ v3_req ]
subjectAltName = @alt_names

[alt_names]
DNS.1=dwh01.example.org
DNS.2=dwh02.example.org
...
```

**Listing 3.** Example configuration for a Certificate Signing Request.


In order to create the actual CSR based on the configuration above, execute the command provided in Listing 4.

```
   $ openssl req -config <CONFIGURATION_FILE> -nodes -new -newkey \
        rsa:2048 -sha256 -out example.org.csr -keyout analytics-server.key
```

**Listing 4.** Command for generating a Certificate Signing Request.

Finally, store the file `example.org.key` in a safe place and send the file `example.org.csr` to your *Certification Authority* (CA) to have it signed.

The configuration of the reverse proxy relies on the server certificate and key to have specific names and to reside in specific locations on the server's filesystem. We further recommend specific file permissions, which are summarized in Table 5.

<table>
<caption><B>Table 5.</B> File locations, ownerships, and permissions for server key and certificate.</caption>
<thead>
<tr class="header">
<th style="text-align: left;"><strong>File</strong></th>
<th style="text-align: left;"><strong>Full Path</strong></th>
<th style="text-align: left;"><strong>Permissions</strong></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">Server key</td>
<td style="text-align: left;"><code>gateway/tls/privkey.pem</code></td>
<td style="text-align: left;"><code>600</code></td>
</tr>
<tr class="even">
<td style="text-align: left;">Server certificate</td>
<td style="text-align: left;"><code>gateway/tls/cert.pem</code></td>
<td style="text-align: left;"><code>644</code></td>
</tr>
</tbody>
</table>

# HowTo ...

## load the i2b2 demo dataset
```
docker exec -ti <name of i2b2 db-container> bash

# Load facts
cd $I2B2_DATA_DIR/Crcdata
sed -i.bak "s/Phoo4eih/$I2B2DEMODATA_PW/" db.properties
ant -f data_build.xml db_demodata_load_data

# Load concepts
cd $I2B2_DATA_DIR/Metadata
sed -i.bak "s/Phoo4eih/$I2B2METADATA_PW/" db.properties
ant -f ant -f data_build.xml db_metadata_load_data

```


## backup & restore data

For backing your DWHs, you need to consider:
* your .env files
* the underlying database instances. Following examples illustrate how to backup and restore the database of instance 01 of i2b2 and tranSMART, respectively, i.e. i2b2-01-db and transmart-01

### i2b2

Backup:
```
docker exec -u postgres i2b2-01-db pg_dump -Fc -c i2b2 > i2b2_01.sqldump
```

Restore:
```
docker exec -u postgres -i i2b2-01-db pg_restore -Fc -c -d i2b2 < i2b2_01.sqldump
```

### tranSMART

Backup:
```
docker exec -u postgres transmart-01 pg_dump -Fc -c transmart > transmart_01.sqldump
```

Restore:
```
docker exec -u postgres -i transmart-01 pg_restore -Fc -c -d transmart < transmart_01.sqldump
```
